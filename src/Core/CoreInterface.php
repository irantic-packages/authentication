<?php

namespace Irantic\Core;


interface CoreInterface
{
    public function authenticate(string $token) :bool;

    public function getUser();
}
