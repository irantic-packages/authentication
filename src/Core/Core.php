<?php

namespace Irantic\Core;

use \Firebase\JWT\JWT;

/**
 * Class Core
 *
 * @param string $user
 * @param string $phone
 *
 * @package Irantic\Core
 *
 */
class Core implements CoreInterface
{
    private $user = [];

    /**
     * Authenticate user via token.
     *
     * @param string $token
     *
     * @return bool
     */
    public function authenticate(string $token) :bool
    {
        $key        = config('jwt.keys.public');
        $algorithms = config('jwt.algorithms');

        try {
            $this->user = JWT::decode($token, $key, $algorithms);

            return true;
        } catch (\Exception $exception) {
            return false;
        }
    }

    /**
     * Get user instance.
     *
     * @return \Illuminate\Support\Collection
     */
    public function getUser()
    {
        return $this->user;
    }
}
