# Auth Package

Created for request authorization between services. 

### Prerequisites

What things you need to install the software and how to install them

```
Php >= 7.2.*
```

### Installing

Install the package via composer:

```
composer require irantic/auth
```

Add the following commands into bootstrap/app.php

```
$app->register(Irantic\Providers\AuthServiceProvider::class);
```

```
$app->configure('jwt');
```

```
$app->routeMiddleware([
    'auth' => Irantic\Middleware\AuthMiddleware::class
]);
```

Then run the command below to publish the dependencies from vendor

```
php artisan vendor:publish --tag=config
```

If any error accorded about vendor:publish command, this reference might be help you
(https://github.com/laravelista/lumen-vendor-publish) 

## Deployment

Now you can easily assign the auth middleware to route groups,
and you can get the instance of authenticated user with

```
\Irantic\Auth::User()
``` 

That contain the all request sender data.

## Built With

* [firebase/php-jwt](https://github.com/firebase/php-jwt) - The jwt library used

## Authors

* **Mehrab Hedayatmanesh** - *Initial work* - [heismehrab](https://gitlab.com/heismehrab)

## License

This project is licensed under the MIT License.