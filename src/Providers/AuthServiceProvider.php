<?php

namespace Irantic\Providers;

use Illuminate\Support\ServiceProvider;
use Irantic\Core\Core;

class AuthServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../config/jwt.php' => app()->basePath('config/jwt.php'),
        ], 'config');
    }

    public function register()
    {
        app()->singleton(
            \Irantic\Core\CoreInterface::class,
            \Irantic\Core\Core::class
        );

        app()->routeMiddleware([
            'auth' => \Irantic\Middleware\AuthMiddleware::class,
        ]);
    }
}
