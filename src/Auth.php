<?php

namespace Irantic;

use Irantic\Core\Core;

final class Auth extends Core
{
    /**
     * Return user instance.
     *
     * @return \Illuminate\Support\Collection
     */
    public static function User()
    {
        return app(\Irantic\Core\CoreInterface::class)->getUser();
    }
}
