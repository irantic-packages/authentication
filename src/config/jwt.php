<?php

return [

    'keys' => [
        'public' => env('AUTH_PUBLIC_KEY', ''),
        'private' => env('AUTH_PRIVATE_KEY', '')
    ],

    'algorithms' => [
        //
    ]
];
