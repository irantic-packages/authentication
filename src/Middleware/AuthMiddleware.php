<?php

namespace Irantic\Middleware;

use Closure;
use Irantic\Core\Core;

class AuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     * @param string|null              $guard
     *
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = NULL)
    {
        if (!$request->headers->has('authorization')) {
            abort(400, 'authentication token not found.');
        }

        $token = $request->header('authorization');
        $token = str_replace('Bearer ', '', $token);

        if (!app(\Irantic\Core\CoreInterface::class)->authenticate($token))
            abort(401, 'unauthorized.');

        return $next($request);
    }
}
